import { parse, Component } from "ical.js"

// Get an ics calendar from url. 
// extract the date and pass it on to callback.
async function getEvent(icsUrl : string, setDateCallback : (date:string) => any) {
  try {
    const response = await fetch(icsUrl);
    const event = await response.text();

    const jcalData = parse(event);
    const vcalendar = new Component(jcalData);
    const vevent = vcalendar.getFirstSubcomponent('vevent');
    const dstart = vevent.getFirstPropertyValue('dtstart');
    const date = new Date(Date.parse(dstart));

    const options : Intl.DateTimeFormatOptions = { timeZone: 'America/Los_Angeles',  
                                                   weekday:  'long',
                                                   month:    'long', 
                                                   day:      'numeric', 
                                                   hour:     'numeric', 
                                                   minute:   'numeric',
                                                 };

    const prettyDate = date.toLocaleString('en-US', options);
    setDateCallback (prettyDate);
  } catch (err) {
    setDateCallback(err);
  }
}

// per: https://stackoverflow.com/questions/23296094/browserify-how-to-call-function-bundled-in-a-file-generated-through-browserify
// and: https://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript

(<any>window).getEvent = getEvent;
